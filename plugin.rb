# name: bid13_plugin
# about: Bid13 Integration for Discourse
# version: 0.15.1.13
# authors: Bid13 Dev

require File.expand_path('../lib/bid13_app/engine', __FILE__)

after_initialize do
	Discourse::Application.routes.append do
		mount ::Bid13App::Engine, at: "/"
	end
end
