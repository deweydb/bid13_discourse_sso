Bid13App::Engine.routes.draw do
	get "bid13/sso" => "bid13#generate_sso_object"
	post "bid13/sso" => "bid13#authenticate_sso_object"

	post "bid13/sso_logout" => "bid13#sso_logout"
end