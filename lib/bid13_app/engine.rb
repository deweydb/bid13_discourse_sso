module Bid13App
  class Engine < ::Rails::Engine
    engine_name 'bid13_app'
    isolate_namespace Bid13App
  end
end