module Bid13App
	require_dependency 'single_sign_on'
	
	class Bid13SingleSignOn < SingleSignOn
		def self.sso_url
			SiteSetting.sso_url
		end

		def self.sso_secret
			SiteSetting.sso_secret
		end

		def self.generate_sso_object(return_path="/")
			sso = new
			sso.nonce = SecureRandom.hex
			sso.register_nonce(return_path="/")
			sso.sso_object
		end

		def sso_object
			payload = Base64.encode64(unsigned_payload)
			hash = {
				:sso => payload,
				:sig => sign(payload)
			}
		end

		def self.verify_request(payload, sso_secret = nil)
			sso = new
			sso.sso_secret = sso_secret if sso_secret

			if sso.sign(payload[:sso]) != payload[:sig]
				diags = "\n\nsso: #{parsed["sso"]}\n\nsig: #{parsed["sig"]}\n\nexpected sig: #{sso.sign(parsed["sso"])}"
				if parsed["sso"] =~ /[^a-zA-Z0-9=\r\n\/+]/m
					raise RuntimeError, "The SSO field should be Base64 encoded, using only A-Z, a-z, 0-9, +, /, and = characters. Your input contains characters we don't understand as Base64, see http://en.wikipedia.org/wiki/Base64 #{diags}"
				else
					raise RuntimeError, "Bad signature for payload #{diags}"
				end
			end

			decoded = Base64.decode64(payload[:sso])
			decoded_hash = Rack::Utils.parse_query(decoded)

			ACCESSORS.each do |k|
				val = decoded_hash[k.to_s]
				val = val.to_i if FIXNUMS.include? k
				if BOOLS.include? k
					val = ["true", "false"].include?(val) ? val == "true" : nil
				end
				sso.send("#{k}=", val)
			end

			decoded_hash.each do |k,v|
				# 1234567
				# custom.
				#
				if k[0..6] == "custom."
					field = k[7..-1]
					sso.custom_fields[field] = v
				end
			end

			sso
		end

		def register_nonce(return_path)
			if nonce
				$redis.setex(nonce_key, NONCE_EXPIRY_TIME, return_path)
			end
		end

		def nonce_valid?
			nonce && $redis.get(nonce_key).present?
		end

		def return_path
			$redis.get(nonce_key) || "/"
		end

		def expire_nonce!
			if nonce
				$redis.del nonce_key
			end
		end

		def nonce_key
			"SSO_NONCE_#{nonce}"
		end

	  def lookup_or_create_user
	    sso_record = SingleSignOnRecord.find_by(external_id: external_id)

	    if sso_record && user = sso_record.user
	      sso_record.last_payload = unsigned_payload
	    else
	      user = match_email_or_create_user
	      sso_record = user.single_sign_on_record
	    end

	    # if the user isn't new or it's attached to the SSO record we might be overriding username or email
	    unless user.new_record?
	      change_external_attributes_and_override(sso_record, user)
	    end

	    if sso_record && (user = sso_record.user) && !user.active
	      user.active = true
	      user.save!
	      user.enqueue_welcome_message('welcome_user')
	    end

	    custom_fields.each do |k,v|
	      user.custom_fields[k] = v
	    end

	    user.admin = admin unless admin.nil?
	    user.moderator = moderator unless moderator.nil?

	    # optionally save the user and sso_record if they have changed
	    user.save!
	    sso_record.save!

	    sso_record && sso_record.user
	  end

	  private

	  def match_email_or_create_user
	    user = User.find_by_email(email)

	    try_name = name.blank? ? nil : name
	    try_username = username.blank? ? nil : username

	    user_params = {
	        email: email,
	        name:  User.suggest_name(try_name || try_username || email),
	        username: UserNameSuggester.suggest(try_username || try_name || email),
	    }

	    if user || user = User.create!(user_params)
	      if sso_record = user.single_sign_on_record
	        sso_record.last_payload = unsigned_payload
	        sso_record.external_id = external_id
	      else
	        user.create_single_sign_on_record(last_payload: unsigned_payload,
	                                          external_id: external_id,
	                                          external_username: username,
	                                          external_email: email,
	                                          external_name: name)
	      end
	    end

	    user
	  end

	  def change_external_attributes_and_override(sso_record, user)
	    if SiteSetting.sso_overrides_email && email != sso_record.external_email
	      # set the user's email to whatever came in the payload
	      user.email = email
	    end

	    if SiteSetting.sso_overrides_username && username != sso_record.external_username && user.username != username
	      # we have an external username change, and the user's current username doesn't match
	      # run it through the UserNameSuggester to override it
	      user.username = UserNameSuggester.suggest(username || name || email)
	    end

	    if SiteSetting.sso_overrides_name && name != sso_record.external_name && user.name != name
	      # we have an external name change, and the user's current name doesn't match
	      # run it through the name suggester to override it
	      user.name = User.suggest_name(name || username || email)
	    end

	    if SiteSetting.sso_overrides_avatar && (
	      avatar_force_update == "true" ||
	      avatar_force_update.to_i != 0 ||
	      sso_record.external_avatar_url != avatar_url)
	      begin
	        tempfile = FileHelper.download(avatar_url, 1.megabyte, "sso-avatar", true)

	        ext = FastImage.type(tempfile).to_s
	        tempfile.rewind

	        upload = Upload.create_for(user.id, tempfile, "external-avatar." + ext, File.size(tempfile.path), { origin: avatar_url })
	        user.uploaded_avatar_id = upload.id

	        unless user.user_avatar
	          user.build_user_avatar
	        end

	        if !user.user_avatar.contains_upload?(upload.id)
	          user.user_avatar.custom_upload_id = upload.id
	        end
	      rescue SocketError
	        # skip saving, we are not connected to the net
	        Rails.logger.warn "Failed to download external avatar: #{avatar_url}, socket error - user id #{ user.id }"
	      ensure
	        tempfile.close! if tempfile && tempfile.respond_to?(:close!)
	      end
	    end

	    # change external attributes for sso record
	    sso_record.external_username = username
	    sso_record.external_email = email
	    sso_record.external_name = name
	    sso_record.external_avatar_url = avatar_url
	  end



	end
end