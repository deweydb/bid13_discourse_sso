module Bid13App
	require 'current_user'
	class ApplicationController < ActionController::Base
		include CurrentUser
		protect_from_forgery with: :exception
		skip_before_action :verify_authenticity_token

		before_action :cors_preflight_check
		before_action :cors_set_access_control_headers


		def cors_set_access_control_headers
			refer_host = URI.parse(request.referrer).host
			if ( refer_host == 'dev.bid13.com' || refer_host == 'www.bid13.com' || refer_host == 'bid13.com' )
				headers['Access-Control-Allow-Origin'] = 'https://' + refer_host
				headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
				headers['Access-Control-Allow-Credentials'] = 'true'
				headers['Access-Control-Request-Method'] = '*'
				headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
				headers['Access-Control-Max-Age'] = "1728000"
			end
		end

		def cors_preflight_check
			if request.method == 'OPTIONS'
				headers['Access-Control-Allow-Origin'] = '*'
				headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
				headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token'
				headers['Access-Control-Max-Age'] = '1728000'
				
				render :text => '', :content_type => 'text/plain'
			end
		end

	end
end