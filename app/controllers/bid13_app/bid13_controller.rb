module Bid13App
	require_dependency 'single_sign_on'

	class Bid13Controller < Bid13App::ApplicationController
		def generate_sso_object
			render json: Bid13SingleSignOn.generate_sso_object
		end

		def authenticate_sso_object
			sso = Bid13SingleSignOn.verify_request(params)

			if !sso.nonce_valid?
				render text: I18n.t("sso.timeout_expired"), status: 500
				return
			end

			return_path = sso.return_path
			sso.expire_nonce!

			begin
				if user = sso.lookup_or_create_user
					if SiteSetting.must_approve_users? && !user.approved?
						render text: I18n.t("sso.account_not_approved"), status: 403
					else
						log_on_user user
					end
					render json: { success: URI.parse(request.referrer).host }
				else
					render text: I18n.t("sso.not_found"), status: 500
				end
			rescue => e
				details = {}
				SingleSignOn::ACCESSORS.each do |a|
					details[a] = sso.send(a)
				end
				Discourse.handle_exception(e, details)

				render text: I18n.t("sso.unknown_error"), status: 500
			end
		end

		def sso_logout
			reset_session
    	log_off_user
    	render nothing: true
		end
	end
end